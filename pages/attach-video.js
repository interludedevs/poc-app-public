import React from "react";
import {
  Banner,
  Card,
  DisplayText,
  Form,
  FormLayout,
  Frame,
  Layout,
  Page,
  PageActions,
  TextField,
  Toast,
} from "@shopify/polaris";
import store from "store-js";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";

const UPDATE_EKO_METADATA = gql`
  mutation productUpdate($input: ProductInput!) {
    productUpdate(input: $input) {
      product {
        metafields(first: 10) {
          edges {
            node {
              id
              namespace
              key
              value
            }
          }
        }
      }
    }
  }
`;

/*
const UPSERT_EKO_METADATA = gql`
  mutation privateMetafieldUpsert($input: PrivateMetafieldInput!) {
    privateMetafieldUpsert(input: $input) {
      privateMetafield {
        namespace
        key
        value
      }
    }
  }
`;

const GET_EKO_METADATA = gql`
query getProduct($id: ID!){
    product(id: $id) {
      title
      metafields(namespace: "eko", first: 10) {
        edges {
          node {
            id
            namespace
            key
            value
          }
        }
      }
    }
  }
`;

const DELETE_EKO_METADATA = gql`
  mutation metafieldDelete($input: MetafieldDeleteInput!) {
    metafieldDelete(input: $input) {
      deletedId
      userErrors {
        field
        message
      }
    }
  }
`;
*/

class AttachVideo extends React.Component {
  state = {
    title: "",
    url: "",
    productId: "",
    urlObj: {},
    showToast: false,
  };

  componentDidMount() {
    this.onMount();
  }

  render() {
    const { title, url, urlObj, productId } = this.state;

    return (
      <Mutation mutation={UPDATE_EKO_METADATA}>
        {(handleSubmit, { error, data }) => {
          const showError = error && (
            <Banner status="critical">{error.message}</Banner>
          );
          const showToast = data && data.productUpdate && (
            <Toast
              content="Sucessfully updated"
              onDismiss={() => this.setState({ showToast: false })}
            />
          );

          const ekoPlayer = url && (
            <Card>
              <iframe
                width="560"
                height="315"
                src={url}
                frameborder="0"
              ></iframe>
            </Card>
          );

          return (
            <Frame>
              <Page>
                <Layout>
                  {showToast}
                  <Layout.Section>{showError}</Layout.Section>
                  <Layout.Section>
                    <DisplayText size="large">{title}</DisplayText>
                    <Form>
                      <Card sectioned>
                        <FormLayout>
                          <FormLayout.Group>
                            <TextField
                              value={url}
                              onChange={this.handleChange("url")}
                              label="Eko video URL"
                              type="url"
                            />
                          </FormLayout.Group>
                        </FormLayout>
                      </Card>
                      {ekoPlayer}
                      <PageActions
                        primaryAction={[
                          {
                            content: "Save",
                            onAction: () => {
                              const productInput = {
                                id: productId,
                                metafields: [
                                  {
                                    id: urlObj.id,
                                    namespace: "eko-video",
                                    key: "url",
                                    value: url,
                                    valueType: "STRING",
                                  },
                                ],
                              };
                              console.log("Saving", productInput);
                              handleSubmit({
                                variables: { input: productInput },
                              });
                            },
                          },
                        ]}
                        secondaryActions={[
                          {
                            content: "Remove Video",
                            onAction: () => {
                              const urlDeleteInput = { id: urlObj.id };
                              handleSubmit({
                                variables: { input: urlDeleteInput },
                              });
                            },
                          },
                        ]}
                      />
                    </Form>
                  </Layout.Section>
                </Layout>
              </Page>
            </Frame>
          );
        }}
      </Mutation>
    );
  }

  handleChange = (field) => {
    return (value) => this.setState({ [field]: value });
  };

  onMount = () => {
    console.log("onMount");
    const item = store.get("item");
    const title = item.title;
    const urlObj = item.metafields.edges[0]
      ? item.metafields.edges[0].node
      : {};
    const url = item.metafields.edges[0]
      ? item.metafields.edges[0].node.value
      : "";
    const productId = item.id;
    console.log(item);
    this.setState({ title, urlObj, url, productId });
  };
}

export default AttachVideo;
