import React from 'react';
import { EmptyState, Layout, Page } from '@shopify/polaris';
import { ResourcePicker, TitleBar } from '@shopify/app-bridge-react';
import store from 'store-js';
import ResourceListWithProducts from '../components/ResourceList';


class Index extends React.Component {
    state = { open: false };
    render() {
        const ids = store.get('ids');
        return (
            <Page>
                <TitleBar
                    title="Products"
                    primaryAction={{
                        content: 'Select products',
                        onAction: () => this.setState({ open: true }),
                    }}
                />
                <ResourcePicker
                    resourceType="Product"
                    showVariants={false}
                    open={this.state.open}
                    onSelection={(resources) => this.handleSelection(resources)}
                    onCancel={() => this.setState({ open: false })}
                    initialSelectionIds={ids ? ids.map((id) => { return {id: id} }) : []}
                />
                {!ids ? (
                    <Layout>
                        <EmptyState
                            heading="Add the eko magic to your products."
                            action={{
                                content: 'Select products',
                                onAction: () => this.setState({ open: true }),
                            }}
                            image="https://cdn.shopify.com/s/files/1/0262/4071/2726/files/emptystate-files.png"
                        >
                            <p>Select products and attach an eko experience.</p>
                        </EmptyState>
                    </Layout>
                ) : (
                    <ResourceListWithProducts />
                )}
            </Page>
        );
    }

    handleSelection = (resources) => {
        const idsFromResources = resources.selection.map((product) => product.id);
        this.setState({ open: false });
        store.set('ids', idsFromResources);
    };

}

export default Index;
