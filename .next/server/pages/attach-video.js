module.exports = /******/ (function (modules) {
  // webpackBootstrap
  /******/ // The module cache
  /******/ var installedModules = require("../ssr-module-cache.js"); // The require function
  /******/
  /******/ /******/ function __webpack_require__(moduleId) {
    /******/
    /******/ // Check if module is in cache
    /******/ if (installedModules[moduleId]) {
      /******/ return installedModules[moduleId].exports;
      /******/
    } // Create a new module (and put it into the cache)
    /******/ /******/ var module = (installedModules[moduleId] = {
      /******/ i: moduleId,
      /******/ l: false,
      /******/ exports: {},
      /******/
    }); // Execute the module function
    /******/
    /******/ /******/ var threw = true;
    /******/ try {
      /******/ modules[moduleId].call(
        module.exports,
        module,
        module.exports,
        __webpack_require__
      );
      /******/ threw = false;
      /******/
    } finally {
      /******/ if (threw) delete installedModules[moduleId];
      /******/
    } // Flag the module as loaded
    /******/
    /******/ /******/ module.l = true; // Return the exports of the module
    /******/
    /******/ /******/ return module.exports;
    /******/
  } // expose the modules object (__webpack_modules__)
  /******/
  /******/
  /******/ /******/ __webpack_require__.m = modules; // expose the module cache
  /******/
  /******/ /******/ __webpack_require__.c = installedModules; // define getter function for harmony exports
  /******/
  /******/ /******/ __webpack_require__.d = function (exports, name, getter) {
    /******/ if (!__webpack_require__.o(exports, name)) {
      /******/ Object.defineProperty(exports, name, {
        enumerable: true,
        get: getter,
      });
      /******/
    }
    /******/
  }; // define __esModule on exports
  /******/
  /******/ /******/ __webpack_require__.r = function (exports) {
    /******/ if (typeof Symbol !== "undefined" && Symbol.toStringTag) {
      /******/ Object.defineProperty(exports, Symbol.toStringTag, {
        value: "Module",
      });
      /******/
    }
    /******/ Object.defineProperty(exports, "__esModule", { value: true });
    /******/
  }; // create a fake namespace object // mode & 1: value is a module id, require it // mode & 2: merge all properties of value into the ns // mode & 4: return value when already ns object // mode & 8|1: behave like require
  /******/
  /******/ /******/ /******/ /******/ /******/ /******/ __webpack_require__.t = function (
    value,
    mode
  ) {
    /******/ if (mode & 1) value = __webpack_require__(value);
    /******/ if (mode & 8) return value;
    /******/ if (
      mode & 4 &&
      typeof value === "object" &&
      value &&
      value.__esModule
    )
      return value;
    /******/ var ns = Object.create(null);
    /******/ __webpack_require__.r(ns);
    /******/ Object.defineProperty(ns, "default", {
      enumerable: true,
      value: value,
    });
    /******/ if (mode & 2 && typeof value != "string")
      for (var key in value)
        __webpack_require__.d(
          ns,
          key,
          function (key) {
            return value[key];
          }.bind(null, key)
        );
    /******/ return ns;
    /******/
  }; // getDefaultExport function for compatibility with non-harmony modules
  /******/
  /******/ /******/ __webpack_require__.n = function (module) {
    /******/ var getter =
      module && module.__esModule
        ? /******/ function getDefault() {
            return module["default"];
          }
        : /******/ function getModuleExports() {
            return module;
          };
    /******/ __webpack_require__.d(getter, "a", getter);
    /******/ return getter;
    /******/
  }; // Object.prototype.hasOwnProperty.call
  /******/
  /******/ /******/ __webpack_require__.o = function (object, property) {
    return Object.prototype.hasOwnProperty.call(object, property);
  }; // __webpack_public_path__
  /******/
  /******/ /******/ __webpack_require__.p = ""; // Load entry module and return exports
  /******/
  /******/
  /******/ /******/ return __webpack_require__(
    (__webpack_require__.s = "./pages/attach-video.js")
  );
  /******/
})(
  /************************************************************************/
  /******/ {
    /***/ "./pages/attach-video.js":
      /*!*******************************!*\
  !*** ./pages/attach-video.js ***!
  \*******************************/
      /*! exports provided: default */
      /***/ function (module, __webpack_exports__, __webpack_require__) {
        "use strict";
        eval(
          '__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _shopify_polaris__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @shopify/polaris */ "@shopify/polaris");\n/* harmony import */ var _shopify_polaris__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var store_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! store-js */ "store-js");\n/* harmony import */ var store_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(store_js__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! graphql-tag */ "graphql-tag");\n/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var react_apollo__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-apollo */ "react-apollo");\n/* harmony import */ var react_apollo__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_apollo__WEBPACK_IMPORTED_MODULE_4__);\nvar _jsxFileName = "/Users/max/eko/eko_poc_app_3.0_public/pages/attach-video.js";\nvar __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\n\n\n\n\n\nconst UPDATE_EKO_METADATA = graphql_tag__WEBPACK_IMPORTED_MODULE_3___default.a`\n    mutation productUpdate($input: ProductInput!) {\n        productUpdate(input: $input) {\n            product {\n                metafields(first: 10) {\n                    edges {\n                        node {\n                            id\n                            namespace\n                            key\n                            value\n                        }\n                    }\n                }\n            }\n        }\n    }\n`;\n/*\nconst UPSERT_EKO_METADATA = gql`\n  mutation privateMetafieldUpsert($input: PrivateMetafieldInput!) {\n    privateMetafieldUpsert(input: $input) {\n      privateMetafield {\n        namespace\n        key\n        value\n      }\n    }\n  }\n`;\n\nconst GET_EKO_METADATA = gql`\nquery getProduct($id: ID!){\n    product(id: $id) {\n      title\n      metafields(namespace: "eko", first: 10) {\n        edges {\n          node {\n            id\n            namespace\n            key\n            value\n          }\n        }\n      }\n    }\n  }\n`;\n\nconst DELETE_EKO_METADATA = gql`\n  mutation metafieldDelete($input: MetafieldDeleteInput!) {\n    metafieldDelete(input: $input) {\n      deletedId\n      userErrors {\n        field\n        message\n      }\n    }\n  }\n`;\n*/\n\nclass AttachVideo extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {\n  constructor(...args) {\n    super(...args);\n\n    _defineProperty(this, "state", {\n      title: \'\',\n      url: \'\',\n      productId: \'\',\n      urlObj: {},\n      showToast: false\n    });\n\n    _defineProperty(this, "handleChange", field => {\n      return value => this.setState({\n        [field]: value\n      });\n    });\n\n    _defineProperty(this, "onMount", () => {\n      console.log(\'onMount\');\n      const item = store_js__WEBPACK_IMPORTED_MODULE_2___default.a.get(\'item\');\n      const title = item.title;\n      const urlObj = item.metafields.edges[0] ? item.metafields.edges[0].node : {};\n      const url = item.metafields.edges[0] ? item.metafields.edges[0].node.value : \'\';\n      const productId = item.id;\n      console.log(item);\n      this.setState({\n        title,\n        urlObj,\n        url,\n        productId\n      });\n    });\n  }\n\n  componentDidMount() {\n    this.onMount();\n  }\n\n  render() {\n    const {\n      title,\n      url,\n      urlObj,\n      productId\n    } = this.state;\n    return __jsx(react_apollo__WEBPACK_IMPORTED_MODULE_4__["Mutation"], {\n      mutation: UPDATE_EKO_METADATA,\n      __self: this,\n      __source: {\n        fileName: _jsxFileName,\n        lineNumber: 99,\n        columnNumber: 7\n      }\n    }, (handleSubmit, {\n      error,\n      data\n    }) => {\n      const showError = error && __jsx(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["Banner"], {\n        status: "critical",\n        __self: this,\n        __source: {\n          fileName: _jsxFileName,\n          lineNumber: 102,\n          columnNumber: 13\n        }\n      }, error.message);\n\n      const showToast = data && data.productUpdate && __jsx(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["Toast"], {\n        content: "Sucessfully updated",\n        onDismiss: () => this.setState({\n          showToast: false\n        }),\n        __self: this,\n        __source: {\n          fileName: _jsxFileName,\n          lineNumber: 105,\n          columnNumber: 13\n        }\n      });\n\n      const ekoPlayer = url && __jsx(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["Card"], {\n        __self: this,\n        __source: {\n          fileName: _jsxFileName,\n          lineNumber: 112,\n          columnNumber: 13\n        }\n      }, __jsx("iframe", {\n        width: "560",\n        height: "315",\n        src: url,\n        frameborder: "0",\n        __self: this,\n        __source: {\n          fileName: _jsxFileName,\n          lineNumber: 113,\n          columnNumber: 15\n        }\n      }));\n\n      return __jsx(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["Frame"], {\n        __self: this,\n        __source: {\n          fileName: _jsxFileName,\n          lineNumber: 118,\n          columnNumber: 13\n        }\n      }, __jsx(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["Page"], {\n        __self: this,\n        __source: {\n          fileName: _jsxFileName,\n          lineNumber: 119,\n          columnNumber: 15\n        }\n      }, __jsx(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["Layout"], {\n        __self: this,\n        __source: {\n          fileName: _jsxFileName,\n          lineNumber: 120,\n          columnNumber: 17\n        }\n      }, showToast, __jsx(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["Layout"].Section, {\n        __self: this,\n        __source: {\n          fileName: _jsxFileName,\n          lineNumber: 122,\n          columnNumber: 19\n        }\n      }, showError), __jsx(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["Layout"].Section, {\n        __self: this,\n        __source: {\n          fileName: _jsxFileName,\n          lineNumber: 125,\n          columnNumber: 19\n        }\n      }, __jsx(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["DisplayText"], {\n        size: "large",\n        __self: this,\n        __source: {\n          fileName: _jsxFileName,\n          lineNumber: 126,\n          columnNumber: 21\n        }\n      }, title), __jsx(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["Form"], {\n        __self: this,\n        __source: {\n          fileName: _jsxFileName,\n          lineNumber: 127,\n          columnNumber: 21\n        }\n      }, __jsx(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["Card"], {\n        sectioned: true,\n        __self: this,\n        __source: {\n          fileName: _jsxFileName,\n          lineNumber: 128,\n          columnNumber: 23\n        }\n      }, __jsx(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["FormLayout"], {\n        __self: this,\n        __source: {\n          fileName: _jsxFileName,\n          lineNumber: 129,\n          columnNumber: 25\n        }\n      }, __jsx(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["FormLayout"].Group, {\n        __self: this,\n        __source: {\n          fileName: _jsxFileName,\n          lineNumber: 130,\n          columnNumber: 27\n        }\n      }, __jsx(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["TextField"], {\n        value: url,\n        onChange: this.handleChange(\'url\'),\n        label: "Eko video URL",\n        type: "url",\n        __self: this,\n        __source: {\n          fileName: _jsxFileName,\n          lineNumber: 131,\n          columnNumber: 29\n        }\n      })))), ekoPlayer, __jsx(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["PageActions"], {\n        primaryAction: [{\n          content: \'Save\',\n          onAction: () => {\n            const productInput = {\n              id: productId,\n              metafields: [{\n                id: urlObj.id,\n                namespace: "eko-video",\n                key: "url",\n                value: url,\n                valueType: \'STRING\'\n              }]\n            };\n            console.log(\'Saving\', productInput);\n            handleSubmit({\n              variables: {\n                input: productInput\n              }\n            });\n          }\n        }],\n        secondaryActions: [{\n          content: \'Remove Video\',\n          onAction: () => {\n            const urlDeleteInput = {\n              id: urlObj.id\n            };\n            handleSubmit({\n              variables: {\n                input: urlDeleteInput\n              }\n            });\n          }\n        }],\n        __self: this,\n        __source: {\n          fileName: _jsxFileName,\n          lineNumber: 141,\n          columnNumber: 23\n        }\n      }))))));\n    });\n  }\n\n}\n\n/* harmony default export */ __webpack_exports__["default"] = (AttachVideo);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9wYWdlcy9hdHRhY2gtdmlkZW8uanM/YmNkZSJdLCJuYW1lcyI6WyJVUERBVEVfRUtPX01FVEFEQVRBIiwiZ3FsIiwiQXR0YWNoVmlkZW8iLCJSZWFjdCIsIkNvbXBvbmVudCIsInRpdGxlIiwidXJsIiwicHJvZHVjdElkIiwidXJsT2JqIiwic2hvd1RvYXN0IiwiZmllbGQiLCJ2YWx1ZSIsInNldFN0YXRlIiwiY29uc29sZSIsImxvZyIsIml0ZW0iLCJzdG9yZSIsImdldCIsIm1ldGFmaWVsZHMiLCJlZGdlcyIsIm5vZGUiLCJpZCIsImNvbXBvbmVudERpZE1vdW50Iiwib25Nb3VudCIsInJlbmRlciIsInN0YXRlIiwiaGFuZGxlU3VibWl0IiwiZXJyb3IiLCJkYXRhIiwic2hvd0Vycm9yIiwibWVzc2FnZSIsInByb2R1Y3RVcGRhdGUiLCJla29QbGF5ZXIiLCJoYW5kbGVDaGFuZ2UiLCJjb250ZW50Iiwib25BY3Rpb24iLCJwcm9kdWN0SW5wdXQiLCJuYW1lc3BhY2UiLCJrZXkiLCJ2YWx1ZVR5cGUiLCJ2YXJpYWJsZXMiLCJpbnB1dCIsInVybERlbGV0ZUlucHV0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQWFBO0FBQ0E7QUFDQTtBQUVBLE1BQU1BLG1CQUFtQixHQUFHQyxrREFBSTtBQUNoQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBakJBO0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLE1BQU1DLFdBQU4sU0FBMEJDLDRDQUFLLENBQUNDLFNBQWhDLENBQTBDO0FBQUE7QUFBQTs7QUFBQSxtQ0FDaEM7QUFDTkMsV0FBSyxFQUFFLEVBREQ7QUFFTkMsU0FBRyxFQUFFLEVBRkM7QUFHTkMsZUFBUyxFQUFFLEVBSEw7QUFJTkMsWUFBTSxFQUFFLEVBSkY7QUFLTkMsZUFBUyxFQUFFO0FBTEwsS0FEZ0M7O0FBQUEsMENBb0d4QkMsS0FBRCxJQUFXO0FBQ3hCLGFBQVFDLEtBQUQsSUFBVyxLQUFLQyxRQUFMLENBQWM7QUFBRSxTQUFDRixLQUFELEdBQVNDO0FBQVgsT0FBZCxDQUFsQjtBQUNELEtBdEd1Qzs7QUFBQSxxQ0F5RzlCLE1BQU07QUFDZEUsYUFBTyxDQUFDQyxHQUFSLENBQVksU0FBWjtBQUNBLFlBQU1DLElBQUksR0FBR0MsK0NBQUssQ0FBQ0MsR0FBTixDQUFVLE1BQVYsQ0FBYjtBQUNBLFlBQU1aLEtBQUssR0FBR1UsSUFBSSxDQUFDVixLQUFuQjtBQUNBLFlBQU1HLE1BQU0sR0FBR08sSUFBSSxDQUFDRyxVQUFMLENBQWdCQyxLQUFoQixDQUFzQixDQUF0QixJQUEyQkosSUFBSSxDQUFDRyxVQUFMLENBQWdCQyxLQUFoQixDQUFzQixDQUF0QixFQUF5QkMsSUFBcEQsR0FBMkQsRUFBMUU7QUFDQSxZQUFNZCxHQUFHLEdBQUdTLElBQUksQ0FBQ0csVUFBTCxDQUFnQkMsS0FBaEIsQ0FBc0IsQ0FBdEIsSUFBMkJKLElBQUksQ0FBQ0csVUFBTCxDQUFnQkMsS0FBaEIsQ0FBc0IsQ0FBdEIsRUFBeUJDLElBQXpCLENBQThCVCxLQUF6RCxHQUFpRSxFQUE3RTtBQUNBLFlBQU1KLFNBQVMsR0FBR1EsSUFBSSxDQUFDTSxFQUF2QjtBQUNBUixhQUFPLENBQUNDLEdBQVIsQ0FBWUMsSUFBWjtBQUNBLFdBQUtILFFBQUwsQ0FBYztBQUFFUCxhQUFGO0FBQVNHLGNBQVQ7QUFBaUJGLFdBQWpCO0FBQXNCQztBQUF0QixPQUFkO0FBQ0QsS0FsSHVDO0FBQUE7O0FBU3hDZSxtQkFBaUIsR0FBRztBQUNsQixTQUFLQyxPQUFMO0FBQ0Q7O0FBRURDLFFBQU0sR0FBRztBQUNQLFVBQU07QUFBRW5CLFdBQUY7QUFBU0MsU0FBVDtBQUFjRSxZQUFkO0FBQXNCRDtBQUF0QixRQUFvQyxLQUFLa0IsS0FBL0M7QUFFQSxXQUNFLE1BQUMscURBQUQ7QUFBVSxjQUFRLEVBQUV6QixtQkFBcEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUNHLENBQUMwQixZQUFELEVBQWU7QUFBRUMsV0FBRjtBQUFTQztBQUFULEtBQWYsS0FBbUM7QUFDbEMsWUFBTUMsU0FBUyxHQUFHRixLQUFLLElBQ3JCLE1BQUMsdURBQUQ7QUFBUSxjQUFNLEVBQUMsVUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQTJCQSxLQUFLLENBQUNHLE9BQWpDLENBREY7O0FBR0EsWUFBTXJCLFNBQVMsR0FBR21CLElBQUksSUFBSUEsSUFBSSxDQUFDRyxhQUFiLElBQ2hCLE1BQUMsc0RBQUQ7QUFDRSxlQUFPLEVBQUMscUJBRFY7QUFFRSxpQkFBUyxFQUFFLE1BQU0sS0FBS25CLFFBQUwsQ0FBYztBQUFFSCxtQkFBUyxFQUFFO0FBQWIsU0FBZCxDQUZuQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFFBREY7O0FBT0EsWUFBTXVCLFNBQVMsR0FBRzFCLEdBQUcsSUFDbkIsTUFBQyxxREFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQ0U7QUFBUSxhQUFLLEVBQUMsS0FBZDtBQUFvQixjQUFNLEVBQUMsS0FBM0I7QUFBaUMsV0FBRyxFQUFFQSxHQUF0QztBQUEyQyxtQkFBVyxFQUFDLEdBQXZEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsUUFERixDQURGOztBQU1BLGFBQ0UsTUFBQyxzREFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQ0UsTUFBQyxxREFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQ0UsTUFBQyx1REFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQ0dHLFNBREgsRUFFRSxNQUFDLHVEQUFELENBQVEsT0FBUjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQ0dvQixTQURILENBRkYsRUFLRSxNQUFDLHVEQUFELENBQVEsT0FBUjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQ0UsTUFBQyw0REFBRDtBQUFhLFlBQUksRUFBQyxPQUFsQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQTJCeEIsS0FBM0IsQ0FERixFQUVFLE1BQUMscURBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxTQUNFLE1BQUMscURBQUQ7QUFBTSxpQkFBUyxNQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FDRSxNQUFDLDJEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FDRSxNQUFDLDJEQUFELENBQVksS0FBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQ0UsTUFBQywwREFBRDtBQUNFLGFBQUssRUFBRUMsR0FEVDtBQUVFLGdCQUFRLEVBQUUsS0FBSzJCLFlBQUwsQ0FBa0IsS0FBbEIsQ0FGWjtBQUdFLGFBQUssRUFBQyxlQUhSO0FBSUUsWUFBSSxFQUFDLEtBSlA7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxRQURGLENBREYsQ0FERixDQURGLEVBYUdELFNBYkgsRUFjRSxNQUFDLDREQUFEO0FBQ0UscUJBQWEsRUFBRSxDQUNiO0FBQ0VFLGlCQUFPLEVBQUUsTUFEWDtBQUVFQyxrQkFBUSxFQUFFLE1BQU07QUFDZCxrQkFBTUMsWUFBWSxHQUFHO0FBQ25CZixnQkFBRSxFQUFFZCxTQURlO0FBRW5CVyx3QkFBVSxFQUFFLENBQ1Y7QUFBRUcsa0JBQUUsRUFBRWIsTUFBTSxDQUFDYSxFQUFiO0FBQWlCZ0IseUJBQVMsRUFBRSxXQUE1QjtBQUF5Q0MsbUJBQUcsRUFBRSxLQUE5QztBQUFxRDNCLHFCQUFLLEVBQUVMLEdBQTVEO0FBQWlFaUMseUJBQVMsRUFBRTtBQUE1RSxlQURVO0FBRk8sYUFBckI7QUFNQTFCLG1CQUFPLENBQUNDLEdBQVIsQ0FBWSxRQUFaLEVBQXNCc0IsWUFBdEI7QUFDQVYsd0JBQVksQ0FBQztBQUNYYyx1QkFBUyxFQUFFO0FBQUVDLHFCQUFLLEVBQUVMO0FBQVQ7QUFEQSxhQUFELENBQVo7QUFHRDtBQWJILFNBRGEsQ0FEakI7QUFrQkUsd0JBQWdCLEVBQUUsQ0FDaEI7QUFDRUYsaUJBQU8sRUFBRSxjQURYO0FBRUVDLGtCQUFRLEVBQUUsTUFBTTtBQUNkLGtCQUFNTyxjQUFjLEdBQUc7QUFBRXJCLGdCQUFFLEVBQUViLE1BQU0sQ0FBQ2E7QUFBYixhQUF2QjtBQUNBSyx3QkFBWSxDQUFDO0FBQ1hjLHVCQUFTLEVBQUU7QUFBRUMscUJBQUssRUFBRUM7QUFBVDtBQURBLGFBQUQsQ0FBWjtBQUdEO0FBUEgsU0FEZ0IsQ0FsQnBCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsUUFkRixDQUZGLENBTEYsQ0FERixDQURGLENBREY7QUE0REQsS0E5RUgsQ0FERjtBQWtGRDs7QUFsR3VDOztBQXFIM0J4QywwRUFBZiIsImZpbGUiOiIuL3BhZ2VzL2F0dGFjaC12aWRlby5qcy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQge1xuICBCYW5uZXIsXG4gIENhcmQsXG4gIERpc3BsYXlUZXh0LFxuICBGb3JtLFxuICBGb3JtTGF5b3V0LFxuICBGcmFtZSxcbiAgTGF5b3V0LFxuICBQYWdlLFxuICBQYWdlQWN0aW9ucyxcbiAgVGV4dEZpZWxkLFxuICBUb2FzdFxufSBmcm9tICdAc2hvcGlmeS9wb2xhcmlzJztcbmltcG9ydCBzdG9yZSBmcm9tICdzdG9yZS1qcyc7XG5pbXBvcnQgZ3FsIGZyb20gJ2dyYXBocWwtdGFnJztcbmltcG9ydCB7IE11dGF0aW9uIH0gZnJvbSAncmVhY3QtYXBvbGxvJztcblxuY29uc3QgVVBEQVRFX0VLT19NRVRBREFUQSA9IGdxbGBcbiAgICBtdXRhdGlvbiBwcm9kdWN0VXBkYXRlKCRpbnB1dDogUHJvZHVjdElucHV0ISkge1xuICAgICAgICBwcm9kdWN0VXBkYXRlKGlucHV0OiAkaW5wdXQpIHtcbiAgICAgICAgICAgIHByb2R1Y3Qge1xuICAgICAgICAgICAgICAgIG1ldGFmaWVsZHMoZmlyc3Q6IDEwKSB7XG4gICAgICAgICAgICAgICAgICAgIGVkZ2VzIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG5vZGUge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZXNwYWNlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWVcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbmA7XG5cbi8qXG5jb25zdCBVUFNFUlRfRUtPX01FVEFEQVRBID0gZ3FsYFxuICBtdXRhdGlvbiBwcml2YXRlTWV0YWZpZWxkVXBzZXJ0KCRpbnB1dDogUHJpdmF0ZU1ldGFmaWVsZElucHV0ISkge1xuICAgIHByaXZhdGVNZXRhZmllbGRVcHNlcnQoaW5wdXQ6ICRpbnB1dCkge1xuICAgICAgcHJpdmF0ZU1ldGFmaWVsZCB7XG4gICAgICAgIG5hbWVzcGFjZVxuICAgICAgICBrZXlcbiAgICAgICAgdmFsdWVcbiAgICAgIH1cbiAgICB9XG4gIH1cbmA7XG5cbmNvbnN0IEdFVF9FS09fTUVUQURBVEEgPSBncWxgXG5xdWVyeSBnZXRQcm9kdWN0KCRpZDogSUQhKXtcbiAgICBwcm9kdWN0KGlkOiAkaWQpIHtcbiAgICAgIHRpdGxlXG4gICAgICBtZXRhZmllbGRzKG5hbWVzcGFjZTogXCJla29cIiwgZmlyc3Q6IDEwKSB7XG4gICAgICAgIGVkZ2VzIHtcbiAgICAgICAgICBub2RlIHtcbiAgICAgICAgICAgIGlkXG4gICAgICAgICAgICBuYW1lc3BhY2VcbiAgICAgICAgICAgIGtleVxuICAgICAgICAgICAgdmFsdWVcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbmA7XG5cbmNvbnN0IERFTEVURV9FS09fTUVUQURBVEEgPSBncWxgXG4gIG11dGF0aW9uIG1ldGFmaWVsZERlbGV0ZSgkaW5wdXQ6IE1ldGFmaWVsZERlbGV0ZUlucHV0ISkge1xuICAgIG1ldGFmaWVsZERlbGV0ZShpbnB1dDogJGlucHV0KSB7XG4gICAgICBkZWxldGVkSWRcbiAgICAgIHVzZXJFcnJvcnMge1xuICAgICAgICBmaWVsZFxuICAgICAgICBtZXNzYWdlXG4gICAgICB9XG4gICAgfVxuICB9XG5gO1xuKi9cblxuY2xhc3MgQXR0YWNoVmlkZW8gZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBzdGF0ZSA9IHtcbiAgICB0aXRsZTogJycsXG4gICAgdXJsOiAnJyxcbiAgICBwcm9kdWN0SWQ6ICcnLFxuICAgIHVybE9iajoge30sXG4gICAgc2hvd1RvYXN0OiBmYWxzZSxcbiAgfTtcblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0aGlzLm9uTW91bnQoKTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IHRpdGxlLCB1cmwsIHVybE9iaiwgcHJvZHVjdElkIH0gPSB0aGlzLnN0YXRlO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxNdXRhdGlvbiBtdXRhdGlvbj17VVBEQVRFX0VLT19NRVRBREFUQX0+XG4gICAgICAgIHsoaGFuZGxlU3VibWl0LCB7IGVycm9yLCBkYXRhIH0pID0+IHtcbiAgICAgICAgICBjb25zdCBzaG93RXJyb3IgPSBlcnJvciAmJiAoXG4gICAgICAgICAgICA8QmFubmVyIHN0YXR1cz1cImNyaXRpY2FsXCI+e2Vycm9yLm1lc3NhZ2V9PC9CYW5uZXI+XG4gICAgICAgICAgKTtcbiAgICAgICAgICBjb25zdCBzaG93VG9hc3QgPSBkYXRhICYmIGRhdGEucHJvZHVjdFVwZGF0ZSAmJiAoXG4gICAgICAgICAgICA8VG9hc3RcbiAgICAgICAgICAgICAgY29udGVudD1cIlN1Y2Vzc2Z1bGx5IHVwZGF0ZWRcIlxuICAgICAgICAgICAgICBvbkRpc21pc3M9eygpID0+IHRoaXMuc2V0U3RhdGUoeyBzaG93VG9hc3Q6IGZhbHNlIH0pfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICApO1xuXG4gICAgICAgICAgY29uc3QgZWtvUGxheWVyID0gdXJsICYmIChcbiAgICAgICAgICAgIDxDYXJkPlxuICAgICAgICAgICAgICA8aWZyYW1lIHdpZHRoPVwiNTYwXCIgaGVpZ2h0PVwiMzE1XCIgc3JjPXt1cmx9IGZyYW1lYm9yZGVyPVwiMFwiPjwvaWZyYW1lPlxuICAgICAgICAgICAgPC9DYXJkPlxuICAgICAgICAgICk7XG5cbiAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPEZyYW1lPlxuICAgICAgICAgICAgICA8UGFnZT5cbiAgICAgICAgICAgICAgICA8TGF5b3V0PlxuICAgICAgICAgICAgICAgICAge3Nob3dUb2FzdH1cbiAgICAgICAgICAgICAgICAgIDxMYXlvdXQuU2VjdGlvbj5cbiAgICAgICAgICAgICAgICAgICAge3Nob3dFcnJvcn1cbiAgICAgICAgICAgICAgICAgIDwvTGF5b3V0LlNlY3Rpb24+XG4gICAgICAgICAgICAgICAgICA8TGF5b3V0LlNlY3Rpb24+XG4gICAgICAgICAgICAgICAgICAgIDxEaXNwbGF5VGV4dCBzaXplPVwibGFyZ2VcIj57dGl0bGV9PC9EaXNwbGF5VGV4dD5cbiAgICAgICAgICAgICAgICAgICAgPEZvcm0+XG4gICAgICAgICAgICAgICAgICAgICAgPENhcmQgc2VjdGlvbmVkPlxuICAgICAgICAgICAgICAgICAgICAgICAgPEZvcm1MYXlvdXQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxGb3JtTGF5b3V0Lkdyb3VwPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxUZXh0RmllbGRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt1cmx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5oYW5kbGVDaGFuZ2UoJ3VybCcpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9XCJFa28gdmlkZW8gVVJMXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJ1cmxcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvRm9ybUxheW91dC5Hcm91cD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvRm9ybUxheW91dD5cbiAgICAgICAgICAgICAgICAgICAgICA8L0NhcmQ+XG4gICAgICAgICAgICAgICAgICAgICAge2Vrb1BsYXllcn1cbiAgICAgICAgICAgICAgICAgICAgICA8UGFnZUFjdGlvbnNcbiAgICAgICAgICAgICAgICAgICAgICAgIHByaW1hcnlBY3Rpb249e1tcbiAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnQ6ICdTYXZlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkFjdGlvbjogKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcHJvZHVjdElucHV0ID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogcHJvZHVjdElkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXRhZmllbGRzOiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBpZDogdXJsT2JqLmlkLCBuYW1lc3BhY2U6IFwiZWtvLXZpZGVvXCIsIGtleTogXCJ1cmxcIiwgdmFsdWU6IHVybCwgdmFsdWVUeXBlOiAnU1RSSU5HJyB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ1NhdmluZycsIHByb2R1Y3RJbnB1dCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVTdWJtaXQoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXJpYWJsZXM6IHsgaW5wdXQ6IHByb2R1Y3RJbnB1dCB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBdfVxuICAgICAgICAgICAgICAgICAgICAgICAgc2Vjb25kYXJ5QWN0aW9ucz17W1xuICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29udGVudDogJ1JlbW92ZSBWaWRlbycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25BY3Rpb246ICgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHVybERlbGV0ZUlucHV0ID0geyBpZDogdXJsT2JqLmlkIH07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVTdWJtaXQoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXJpYWJsZXM6IHsgaW5wdXQ6IHVybERlbGV0ZUlucHV0IH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIF19XG4gICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPC9Gb3JtPlxuICAgICAgICAgICAgICAgICAgPC9MYXlvdXQuU2VjdGlvbj5cbiAgICAgICAgICAgICAgICA8L0xheW91dD5cbiAgICAgICAgICAgICAgPC9QYWdlPlxuICAgICAgICAgICAgPC9GcmFtZT5cbiAgICAgICAgICApO1xuICAgICAgICB9fVxuICAgICAgPC9NdXRhdGlvbj5cbiAgICApO1xuICB9XG5cbiAgaGFuZGxlQ2hhbmdlID0gKGZpZWxkKSA9PiB7XG4gICAgcmV0dXJuICh2YWx1ZSkgPT4gdGhpcy5zZXRTdGF0ZSh7IFtmaWVsZF06IHZhbHVlIH0pO1xuICB9O1xuXG5cbiAgb25Nb3VudCA9ICgpID0+IHtcbiAgICBjb25zb2xlLmxvZygnb25Nb3VudCcpO1xuICAgIGNvbnN0IGl0ZW0gPSBzdG9yZS5nZXQoJ2l0ZW0nKTtcbiAgICBjb25zdCB0aXRsZSA9IGl0ZW0udGl0bGU7XG4gICAgY29uc3QgdXJsT2JqID0gaXRlbS5tZXRhZmllbGRzLmVkZ2VzWzBdID8gaXRlbS5tZXRhZmllbGRzLmVkZ2VzWzBdLm5vZGUgOiB7fTtcbiAgICBjb25zdCB1cmwgPSBpdGVtLm1ldGFmaWVsZHMuZWRnZXNbMF0gPyBpdGVtLm1ldGFmaWVsZHMuZWRnZXNbMF0ubm9kZS52YWx1ZSA6ICcnOyAgICBcbiAgICBjb25zdCBwcm9kdWN0SWQgPSBpdGVtLmlkO1xuICAgIGNvbnNvbGUubG9nKGl0ZW0pO1xuICAgIHRoaXMuc2V0U3RhdGUoeyB0aXRsZSwgdXJsT2JqLCB1cmwsIHByb2R1Y3RJZCB9KTtcbiAgfTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgQXR0YWNoVmlkZW87Il0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/attach-video.js\n'
        );

        /***/
      },

    /***/ "@shopify/polaris":
      /*!***********************************!*\
  !*** external "@shopify/polaris" ***!
  \***********************************/
      /*! no static exports found */
      /***/ function (module, exports) {
        eval(
          'module.exports = require("@shopify/polaris");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAc2hvcGlmeS9wb2xhcmlzXCI/YTYyMyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSIsImZpbGUiOiJAc2hvcGlmeS9wb2xhcmlzLmpzIiwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQHNob3BpZnkvcG9sYXJpc1wiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///@shopify/polaris\n'
        );

        /***/
      },

    /***/ "graphql-tag":
      /*!******************************!*\
  !*** external "graphql-tag" ***!
  \******************************/
      /*! no static exports found */
      /***/ function (module, exports) {
        eval(
          'module.exports = require("graphql-tag");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJncmFwaHFsLXRhZ1wiP2Y4YjciXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEiLCJmaWxlIjoiZ3JhcGhxbC10YWcuanMiLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJncmFwaHFsLXRhZ1wiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///graphql-tag\n'
        );

        /***/
      },

    /***/ react:
      /*!************************!*\
  !*** external "react" ***!
  \************************/
      /*! no static exports found */
      /***/ function (module, exports) {
        eval(
          'module.exports = require("react");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdFwiPzU4OGUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEiLCJmaWxlIjoicmVhY3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///react\n'
        );

        /***/
      },

    /***/ "react-apollo":
      /*!*******************************!*\
  !*** external "react-apollo" ***!
  \*******************************/
      /*! no static exports found */
      /***/ function (module, exports) {
        eval(
          'module.exports = require("react-apollo");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC1hcG9sbG9cIj8yMGU2Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBIiwiZmlsZSI6InJlYWN0LWFwb2xsby5qcyIsInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LWFwb2xsb1wiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///react-apollo\n'
        );

        /***/
      },

    /***/ "store-js":
      /*!***************************!*\
  !*** external "store-js" ***!
  \***************************/
      /*! no static exports found */
      /***/ function (module, exports) {
        eval(
          'module.exports = require("store-js");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJzdG9yZS1qc1wiP2QxYWIiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEiLCJmaWxlIjoic3RvcmUtanMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJzdG9yZS1qc1wiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///store-js\n'
        );

        /***/
      },

    /******/
  }
);
